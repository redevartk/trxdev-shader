# EVO Shader Mod V1.1 "Unbelievable" (UPDATED)

![1703124870674](documentations/image/README/1703124870674.png)

## ***EVO V1.1 ChangeLog Feature : (MCPE 0.16.0+)***

+Contain Old Feature EVO V1.0

1. Add DOF blue fog near distance
2. Add realistic orange-yellow heat torch light
3. Now torch is not lighty in out side of shadow
4. Add torch blur effect near fog
5. Add reddish torch light behind fog when rainy
6. Add adaptive torch light Behind Light And Shadow
7. Add realistic darkness in shadowing
8. Add realistic darkness night light
9. Add ambient fog when sunset or sunrise
10. Add natural water color
11. Add calm wavy water and grass wavy
12. Mob now rendered behind fog
13. Add optimized lighting on mob
14. Add realistic cloud and height
15. now water with X(Horizontal) POSITION will render fully
16. Add ranged blue fog in long distance
17. Add realistic darkness rain
18. Add absolute darkness In depth of shadowing (cave)
19. Add moon ambient fog when night
20. When sunrise or sunset world will increase the bright if we look at sun
21. Now brightness slider is useless, with the world is optimized.
22. Always "Find And Search More"

## ***EVO V1.1 [R2] ChangeLog Feature : (1.0.0+)***

1. Bug Fix

## Want Full Beauty ?

Explore medieval map ,castle ,forest or other good map with EVO Shader ( ULTRA ) !

## ***ScreenShot :***

![1703125035104](documentations/image/README/1703125035104.png)

![1703125091358](documentations/image/README/1703125091358.png)

![1703125125311](documentations/image/README/1703125125311.png)

![1703125132125](documentations/image/README/1703125132125.png)

![1703125178456](documentations/image/README/1703125178456.png)

***Follow My Twitter : @TRXDev***

*Folder Inside List :* ***EVO Shader Mod V1.1.zip >> Lite,Standart,Ultra >> EVO Shader Mod V1.1 ( Ultra/Standart/Lite).zip***

## **How To Install Shader :**

[Only for Normal MCPE] : For Instal you only need to use zip as texture pack (Dont use zip because it will make your mcpe lag 2X MORE) inside resource_packs folder

Example : EVO Shader Mod V1.1(Ultra) <<< Folder Inside resource_packs folder.

>> need extract/inner folder<<
>>

[Only for .mcpack files] : For Install .MCPack files make sure you copy the file in open card exam : /sdcard/ "file"

Don't instal it inside folder because it won't work.

[Only For Block Launcher] Install it By using current zip

Example: EVO Shader Mod V1.1 (Ultra).zip in texture pack menu.

**Another Useful Install Guide :**

Android : [How To Install (By MCPEDL)](https://www.minecraftforum.net/linkout?remoteUrl=http%253a%252f%252fmcpedl.com%252fhow-to-install-minecraft-pe-texture-packs-for-android%252f)

IOS : [How To Install (By MCPEDL)](https://www.minecraftforum.net/linkout?remoteUrl=http%253a%252f%252fmcpedl.com%252fhow-to-install-minecraft-pe-texture-packs-for-ios%252f)

## **System Requirement :**

```
Check The Your Phone GFLOPS : CHECK


System Requirement (Minimum) :

OS: Android/IOS

GFLOPS : 30+

CPU : Quad OR Octa 1,1+ GHz.

GPU : - Adreno 305

- Mali 400MP

- Tegra 4+

- (PowerVR) Unknown Maybe : SGX 544MP


System Requirement (Recommend) :

OS: Android/IOS

GFLOPS : 75+

CPU : Quad OR Octa 1,7+ GHz

GPU: - Adreno 405

- Mali T860

- Tegra K1+

- (PowerVR) Unknown Maybe : G6200
```


## ***Current Bug List :***

1. Sometimes world flickering when sunset or sunrise
2. Water wIth Y(vertical) position is bug on texture

## ***Some Usefull Info :***

1. You need to wait shader loading max **1 minute** if you have (OpenGL ES 2.0) or old phone


## Download :

Download (0.16.0+) :

[V1 Download]

Download (1.0.0+) :

[V2 Download]


***Note*** *:* IF YOU HAVE PROBLEM WITH DOWNLOAD PLEASE DONT USE IDM OR OTHER THING,JUST USE DEFAULT BROWSER BECAUSE SOMETIMES IT HAVE PROBLEM WITH SECURE CONNECTION**.**

**IF YOU FIND BUG ON SHADER ,Please comment in comment section below**

Please for GPU:

1. PowerVR

Test it and report by reply/comment so i will know if this shader support that devices
