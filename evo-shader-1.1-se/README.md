# EVO Shader Mod V1.1 (Special Editon)

**+Contain EVO V1.1 Features**

(Support Minecraft PE 1.2.6+)

1.Add better sunlight and nightlight
2.Add realistic optimize cave torch color
3.Add light focus
4.Worldlight now more optimize
5.Fix nether light blurring
6.Fix distance fog water
7.Fix rainy weather fog
8.Always "find and search more"

**-Compability Fix :**

1.Now compatible with iOS devices
2.Fix lag and bug on MALI GPU devices

**-Current Compability And Bug List :**

1.Adreno 530 device unknown not responding.
2.Cloud rollback to old evo v1.0 to make sure there's no bug/compability bug.
3.Sometimes world flickering when sunset or sunrise

**Screenshot :**


**System Requirement :**

```
Check The Your Phone GFLOPS : CHECK


System Requirement (Minimum) :
OS: Android/IOS (Only some supported)
GPU FLOPS : 30+
CPU : Quad OR Octa 1,1+ GHz.


System Requirement (Recommend) :
OS: Android/IOS (Only some supported)
GPU FLOPS : 75+
CPU : Quad OR Octa 1,7+ GHz
```


Twitter : @TRXDev

please comment below if you find bug and other compability problem.
Enjoy the Shader 
