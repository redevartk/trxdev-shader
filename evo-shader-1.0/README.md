# EVO Shader Mod V1.0 "Gen System"

![1703126245257](documentations/image/README/1703126245257.png)

## ChangeLog Feature V1.0:

1.Optimized beautiful world light
2.DOF blue shadow in distance
3.Realistic day and night cycle
4.Add heat feel in light source
5.Add glow and blur light
6.Realistic water color and animated
7.Add soft blue fog in distance
8.Add lighting on mob and player
9.Add blue soft gradient on shadow
10.Add dark side on cloud
11.realistic sky light
12.Now water will wavy if in big water exam: sea,big lake
13.Find and search more ![:D](https://media.minecraftforum.net/avatars/0/8/635356669595335315.png ":D")

**Screenshot:**

![1703126252710](documentations/image/README/1703126252710.png)

![1703126258442](documentations/image/README/1703126258442.png)

![1703126264811](documentations/image/README/1703126264811.png)

![1703126270414](documentations/image/README/1703126270414.png)

![1703126276386](documentations/image/README/1703126276386.png)

Please read the licence inside shader before you do something with shader except only personal use.

**Contact:**

Email : ~~trx@tech-center.com~~
Twitter : @TRXDev

If you find bug please comment in down
ENJOY !
