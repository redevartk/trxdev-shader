# EVO Shader V1.3 "A New Future"

![1703126455620](documentations/image/README/1703126455620.png)

**A new future** for mcpe shader modding, enjoy realistic world feeling and many feature arrive for mcpe shader community including realistic lighting, new optimized fbm cloud and many more.

**SCREENSHOT :**

![1703126476608](documentations/image/README/1703126476608.png)

![1703126484412](documentations/image/README/1703126484412.png)

![1703126493038](documentations/image/README/1703126493038.png)

![1703126499822](documentations/image/README/1703126499822.png)


**FEATURE LIST :**

* Almost All EVO SHADER V1.2 Feature
* New Life Lighting
* Shadow Animation on Leaves
* Cloud Shadow for World
* Optimial Vibrant Color
* Living Torch Light Effect and Color
* Realistic Fancy Optimized FBM Sky-Cloud
* 2D Plane Cloud and Sky Reflection on Water
* Fancy Pre-Raymarch Wavy on Water
* Fancy Realistic Sun and Moon
* Optimal darkness for night and cave
* Atmospheric Ambience for Nether and The End
* Under Water Lighting and Water Caustics Effect
* Water Ripple when Rain (DISABLED, too lag, advance user only)
* Realistic Ambience for sky and cloud
* Realistic sun and moon light glare and scattering
* Mob/Player Lighting Direction
* Discover More . . .


the feature already canceled for performance reason (just call it still in development feature)

**SOME CANCELED FEATURE :**

* Realistic Fog when Rain
* Shadow and Water Atmospheric Fog
* Raymarching Water Reflection

**SOME BUGS :**

* Sometimes when look at sky at 90 degrees, cause the world to turn into dusk cycles
* Slab is dark (covered in shadow)
* Stairs doesn’t cast first lvl shadow
* Sometimes it needs at least some second to reload shader per-region e.g : when underwater, nether, end
* Rain still have strong shadow
* Night Potion Doesn’t Work
* Nether will switch shader over special dark or bright area

shader will have 5 settings, you can swtich it inside resource pack folder (gear logo). Accuracy is where the shader will maintain the wavy and shadow or some settiings so when you play in long time or restart the mcpe (not kill task), the shader will still in accuracy without being blurry, stutter or lose the quality

**SHADER SETTINGS**** :**

* LOW : most highest and optimized FPS, but sacrifice some feature and accuracy
* LOW_STANDART : same as standart but sacrifice accuracy. Save FPS
* STANDART : a standart one, moderate performance and good accuracy
* EXTREME : all feature is on, good accuracy, but the slowest one
* CUSTOM (default) : same as standart, you can edit the settings in : “/resource_packs/”MY EVO SHADER”/subpacks/custom/shaders/glsl/settings/settings.h”

there’s some setting that not mentioned, like accuracy and performance mode, probably you can tweak it on custom settings for the best one. EVERYTIME YOU CHANGE SETTINGS, PLEASE RESTART MCPE (KILL TASK) AND OPEN AGAIN MCPE.

**SYSTEM REQUIREMENTS (MINIMUM) :**

* Device : ANDROID & IOS (not sure, but pls test it) ONLY FOR NOW
* GPU : ALL Mobile GPU (not sure but pls test it on your phone)
* OpenGL : support high precision is optional but very needed
* Rank : 840 +-
* please check your gpu rank according to : [Smartphone Graphics Cards - Benchmark List - NotebookCheck.net Tech](https://www.minecraftforum.net/linkout?remoteUrl=https%253a%252f%252fwww.notebookcheck.net%252fSmartphone-Graphics-Cards-Benchmark-List.149363.0.html)

**NOTE FOR YOU WHO USE MY SHADER :**

* Please **set**  **MCPE brigtness to 0** , brightness will have no effect on shader, it will create bug if you set it more, just leave it to 0
* if shader transparent or force close, that mean the shader isn’t support for your device
* if you encounter bug or crash, pls comment or inform me in twitter @TRXDev

**NOTE FOR SHADER DEVELOPER :**

* if you want copy paste my shader or do something with it, please read the license inside shader.

**INSTALLATION :**

CLICK .mcpack file using extrenal explorer or internal (if supported) and it will automatically open the MCPE

**DON’T FORGET :**

to SHARE shader with other community or your friend, also I hope you all make the video for my shader, I will gladly watch it ![:)](https://media.minecraftforum.net/avatars/0/14/635356669596273706.png ":)")

***ENJOY THE SHADER, GOOD LUCK ![:)](https://media.minecraftforum.net/avatars/0/14/635356669596273706.png ":)")***
