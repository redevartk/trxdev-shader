#version 100

//Reality Revolx 2.0by JoseMan0209
//True or false for enable or disable features


/*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/

varying vec2 uv0;
varying vec2 uv1;
varying vec4 color;
varying vec4 fogColor;
varying vec4 fardark;
varying vec4 fogNear;

uniform sampler2D TEXTURE_0;
uniform sampler2D TEXTURE_1;
uniform sampler2D TEXTURE_2;
uniform highp float TIME;

highp float shadow = 1.5;

vec4 nightLight(vec4 color, float null){
	float map = (color.r + color.g + color.b) / 55.5;
return mix(color, vec4(map, map, map * 4.5, color.a), null);
}

vec4 dayLight(vec4 color, float null){
return color;
}

vec4 sunLight(vec4 color, float null){
	float map = (color.r + color.g + color.b) /7.0;
return mix(color, vec4(map * 4.5, map, map, color.a), null);
}

vec4 lightMap(vec4 color, vec2 curr, float setLight){
  	 float day = curr.y * (1.0 - curr.x) * 0.75 * setLight;
    float night = curr.y * (1.0 - curr.x) * 0.85 * (1.0 - setLight);
    float sun = (0.5 - abs(0.5 - setLight)) * curr.y;
return sunLight(dayLight(nightLight(color, night),day),sun);
}

void main()
{
	vec4 texture = texture2D( TEXTURE_0, uv0 );
    vec4 opacity = texture2D( TEXTURE_1, uv1 );
	 vec4 transf = texture2D( TEXTURE_1, vec2( 0.0, 1.0));
    vec4 inColor = color;
    vec2 tex = floor(vec2(uv0.x * 32.0, uv0.y * 16.0));

    float setLight = (transf.r - 0.5) / 0.5;
    setLight = max(0.0, min(1.0, setLight));

//Water advance color control 

#ifdef NEAR_WATER
   #ifdef FANCY
if(tex.x < 15.0){
     texture = texture2D( TEXTURE_1, uv1);
     texture.rgb *= vec3(0.2, 0.5, 0.55);
     texture.a = 0.65; }

    #endif
#endif

#ifdef SEASONS_FAR
	texture.a = 0.5;
	inColor.b = 1.0;
#endif

#ifdef ALPHA_TEST
	if(texture.a < 0.5)
	 	discard;

#endif
    
#ifndef SEASONS

#if !defined(ALPHA_TEST) && !defined(BLEND)
	texture.a *= 1.0;
#elif defined(BLEND)
	texture.a *= 0.9;

#endif	

	texture.rgb *= inColor.rgb;
#else
	vec2 uv = inColor.xy;
	uv.y += 1.0 / 512.0;
	texture.rgb *= mix(vec3(1.0,1.0,1.0), texture2D( TEXTURE_2, uv).rgb * 2.0, inColor.b);
	texture.rgb *= inColor.aaa;
	texture.a = 0.0;
	#endif

   texture.rgb *= 0.5;
//texture.a *= 1.0; ALPHA_ way

   opacity.rgb *= 2.5;
//opacity.a *= 1.0; ALPHA_ way


vec4 torchr = vec4 (1.0, 0.05, -0.4,0.0);

texture+= vec4(torchr)*pow(uv1.x * 0.65, 3.5);
opacity+= vec4(torchr)*pow(uv1.x * 0.65, 3.5);

   vec2 blight = 1.0 - abs(sign(vec2(inColor.b - inColor.g, inColor.g - inColor.b)));
   float bcoord = blight.x * blight.y;
   float Il = bcoord * clamp(uv1.x, 0.8, 1.0);
   float II = 1.1 - abs(sign(Il));

   texture *= opacity;
   texture.rgb *= vec3( 1.0 * Il + II, 1.0 * Il + II, 1.0 * Il + II );

if(uv1.y < 0.90674){shadow = 0.25 + uv1.x * 0.5;}
if(uv1.y < 0.90674){texture.b *= 1.35;}
texture.rgb *= min(0.9, shadow);

	gl_FragColor = lightMap(texture, uv1, setLight);
}
