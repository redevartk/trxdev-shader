#version 100


/*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/


uniform MAT4 WORLDVIEWPROJ;
uniform vec4 FOG_COLOR;
uniform vec4 CURRENT_COLOR;
uniform float RENDER_DISTANCE;
uniform POS3 CHUNK_ORIGIN;
uniform highp float TIME;

attribute mediump vec4 POSITION;
attribute vec2 TEXCOORD_0;

varying vec2 uv;

void main()
{
  
POS4 pos = WORLDVIEWPROJ * POSITION;

float depth = pos.z * RENDER_DISTANCE;

vec4 sunmoon = POSITION;
    	
   sunmoon.xyz *=vec3(2.0,-1.0,2.0);

	gl_Position = 	WORLDVIEWPROJ * sunmoon;

    uv = TEXCOORD_0;
}

















