#version 100

/*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/


const bool  wavyClouds			= false;

uniform MAT4 WORLDVIEWPROJ;
uniform float RENDER_DISTANCE;
uniform vec4 FOG_COLOR;
uniform vec4 CURRENT_COLOR;
uniform POS3 CHUNK_ORIGIN;
uniform POS3 VIEW_POS;
uniform highp float TIME;

attribute mediump vec4 POSITION;
attribute vec4 COLOR;

varying vec4 color;

const float fogNear = 0.0;

const vec3 inverseLightDirection = vec3(0.12, 0.18, 0.0);
const float ambient = 9.5;

void main()
{

vec4 cloud = POSITION;

POS4 pos = WORLDVIEWPROJ * cloud;

vec3 wav = POSITION.xyz * 9.0;

const float cloudspeed = 0.5;
const float shadowintensify =6.0;

 const float waveHeight 		= 999.04;
	const float waveSpeed		= 3.141569;
	const float waveResolution	= 5.0;

	if (wavyClouds) {
	gl_Position.y += waveHeight * sin(TIME * waveSpeed + wav.x);
 gl_Position.y += waveHeight * sin(TIME * waveSpeed + wav.z);
}

color.b +=cos(TIME * cloudspeed+((wav.x+wav.x+wav.y+wav.z+wav.x+wav.y+wav.y+wav.z+wav.x+wav.y+wav.z+wav.z)*cos(wav.z)))/6.0;

color.a +=cos(TIME * 0.1+((wav.x+wav.x+wav.y+wav.z+wav.x+wav.y+wav.y+wav.z+wav.x+wav.y+wav.z+wav.z)*cos(wav.x)))/1.0;

gl_Position = pos;


float depth = pos.z / RENDER_DISTANCE;
float fog = max(depth - fogNear, 0.0);

vec4 Blur =FOG_COLOR;
          Blur.r*=max(1.0,3.0);
          Blur.g*=max(1.0,1.1);
          Blur.b*=max(1.0,1.0);

 	color = mix(Blur * color.b, FOG_COLOR, depth);


 	color.a *= 1.0 - fog;

}