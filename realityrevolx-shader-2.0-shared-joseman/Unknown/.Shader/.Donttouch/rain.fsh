#version 100


/*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/


uniform sampler2D TEXTURE_0;
uniform sampler2D TEXTURE_1;

varying vec2 uv;
varying vec4 color;
varying vec4 worldPosition;
varying vec4 fogColor;

void main()
{
	vec4 albedo = texture2D( TEXTURE_0, uv);

#ifdef ALPHA_TEST
	if (albedo.a < 1.0)
		discard;
#endif

	albedo.a *= color.a * 2.0;

	vec2 occlusionUV = worldPosition.xz;
	vec4 occlusionTexture = texture2D( TEXTURE_1, occlusionUV);

	occlusionTexture.a *= 255.0;	// Alpha stores height of world

	// clamp the uvs
	vec2 uvclamp = clamp(occlusionUV, 0.0, 1.0);
	if (uvclamp.x == occlusionUV.x && uvclamp.y == occlusionUV.y && worldPosition.y < occlusionTexture.a) {
		albedo.a = 0.0;
	}

	float mixAmount = clamp ( (worldPosition.y - occlusionTexture.a)*9.0, 0.0, 1.0);
	vec3 lighting = mix(occlusionTexture.rgb, color.rgb, mixAmount);

albedo.r = 2.5;
albedo.g = 2.5;
albedo.b = 3.0;

	gl_FragColor = albedo * 0.25;
	gl_FragColor.rgb *= lighting.rgb;

	//apply fog
	gl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor.rgb, fogColor.a );

}


