#version 100

/*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/

//Copyright © JoseMan0209
//JoseMan0209 Source

const bool blueMoon = false;
const bool redMoon =true;
const bool witheMoon = false;

varying vec2 uv;

uniform vec4 FOG_COLOR;
uniform vec4 CURRENT_COLOR;
uniform sampler2D TEXTURE_0;

uniform highp float TIME;
uniform POS3 CHUNK_ORIGIN;

void main()
{
	vec4 diffuse = texture2D( TEXTURE_0, uv );

#ifdef ALPHA_TEST
	if(diffuse.a < 0.5)
 discard;
#endif


if (blueMoon) {
     vec4 sun = vec4(1.0, 1.0, 1.3,1.0);
	    gl_FragColor = diffuse * sun;
}

if (redMoon) {
     vec4 sun = vec4(1.0, 0.7, 0.5,1.0);
	    gl_FragColor = diffuse * sun;
}

if (witheMoon) {
     vec4 sun = vec4(1.5, 1.5, 1.5,1.0);
	    gl_FragColor = diffuse * sun;
}


	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
