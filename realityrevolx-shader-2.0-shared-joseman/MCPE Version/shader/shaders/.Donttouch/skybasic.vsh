#version 100

/*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/

uniform MAT4 WORLDVIEWPROJ;
uniform vec4 FOG_COLOR;
uniform vec4 CURRENT_COLOR;
uniform highp float TIME; 
uniform POS3 CHUNK_ORIGIN;

attribute highp vec4 POSITION;
attribute vec4 COLOR;

varying vec4 color;

const float fogNear = 0.3;

float calc_weather_value(){
 //float tmp = min(1.0, max(0.0, (sin(TIME * 0.008 + 0.2j)) * 4.0));
 return 0.0;
}

void main()
{

vec3 wav = POSITION.xyz;
         wav.xyz *= vec3 (0.3,0.3,0.3);

const float cloudspeed = 6.0;
const float shadowintensify =2.0;

gl_Position.x +=cos(TIME * cloudspeed+((wav.x+wav.x+wav.y+wav.z+wav.x+wav.y+wav.y+wav.z+wav.x+wav.y+wav.z+wav.z)*cos(wav.x)))/shadowintensify;

    gl_Position = WORLDVIEWPROJ * POSITION;

    vec4 clr = CURRENT_COLOR;
    clr.r = max(clr.r, 0.2);
    clr.g = max(clr.g,0.2);
    clr.b = max(clr.b, 0.25);
    
    vec4 gray = vec4(clr.r, clr.r, clr.r, clr.a);
    vec4 blue = vec4(2.0,3.0,5.0,1.0);

    color = mix(mix(clr * 0.12, gray, calc_weather_value()) * blue, FOG_COLOR, COLOR.r );
}
