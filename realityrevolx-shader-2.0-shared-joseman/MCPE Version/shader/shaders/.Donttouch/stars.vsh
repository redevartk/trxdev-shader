
 /*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/


uniform MAT4 WORLDVIEWPROJ;
uniform highp float TIME;
uniform vec4 FOG_COLOR;
uniform vec2 FOG_CONTROL;
uniform float RENDER_DISTANCE;
uniform vec4 CURRENT_COLOR;	      
uniform POS3 CHUNK_ORIGIN;
uniform POS3 VIEW_POS;
uniform float FAR_CHUNKS_DISTANCE;

attribute vec4 COLOR;
attribute highp vec4 POSITION;


varying vec4 color;

void main()
{

color.rgb *=sin(TIME*0.1);

vec4 star = POSITION;
          star.xyz *=vec3(0.5,0.5,0.5);


vec3 wav = POSITION.xyz;
         wav.xyz *= vec3 (0.3,0.3,0.3);

const float starspeed = 9.0;
const float shadowintensify =1.0;

color.rgb +=cos(TIME * starspeed+((wav.x+wav.x+wav.y+wav.z+wav.x+wav.y+wav.y+wav.z+wav.x+wav.y+wav.z+wav.z)*cos(wav.y)))/shadowintensify;

    gl_Position = WORLDVIEWPROJ * star;
}





















