#version 100

uniform MAT4 WORLDVIEWPROJ;

attribute mediump vec4 POSITION;
uniform highp float TIME;

void main()
{
    vec4 pos = POSITION;
    float len = abs(sin(TIME*0.01)+0.0);
    pos.z += len / 1.0;
    pos.z *= len * 2.0;
    pos.x += len / 8.0;
    pos.x *= len;

  gl_Position = WORLDVIEWPROJ * pos;
}