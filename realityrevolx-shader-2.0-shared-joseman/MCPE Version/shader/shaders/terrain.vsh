#version 100

//Reality Revolx 2.0by JoseMan0209
//True or false for enable or disable features

const bool wavyLeaves = true;
const bool wavyWater = true;
const bool wavyWaterShadows = true;
const bool wavyWaterAlpha =true;

const bool WaterColor = true;

/*
//Reality Revolx 2.0

//Copyright © JoseMan0209
//JoseMan0209 Source

//Copyright © T.R.X
//Contain T.R.X Source

//Contain Jocopa3 Source 

███████▒▒▒▒▒███████       
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
███████▒▒▒▒▒███████    
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██
██▒▒▒▒▒██▒▒▒██▒▒▒▒▒██

Before editing anything here make sure you've read The licence agreement, which you accepted by downloading	my shaderpack

The agreement can be found inside the zip file:
Reality Revolx 2.0 license.txt
*/


uniform MAT4 WORLDVIEWPROJ;
uniform vec4 FOG_COLOR;
uniform vec2 FOG_CONTROL;
uniform float RENDER_DISTANCE;
uniform vec2 VIEWPORT_DIMENSION;
uniform vec4 CURRENT_COLOR;		
uniform POS3 CHUNK_ORIGIN;
uniform POS3 VIEW_POS;
uniform float FAR_CHUNKS_DISTANCE;

attribute POS4 POSITION;
attribute vec4 COLOR;
attribute vec2 TEXCOORD_0;
attribute vec2 TEXCOORD_1;
uniform highp float TIME;
uniform sampler2D TEXTURE_0;
uniform sampler2D TEXTURE_1;

uniform sampler2D s_texture;

varying vec2 uv0;
varying vec2 uv1;
varying vec4 color;

varying vec2 v_texCoord;
varying vec2 v_blurTexCoords[14];

#ifdef FOG
	varying vec4 fogColor;
#endif

const float rA = 2.0; 
const float rB = 2.0; 
vec3 UNIT_Y = vec3(0,0,0);
const float DIST_DESATURATION = 0.1196078;

void main()
{

    POS4 pos = WORLDVIEWPROJ * POSITION;
    gl_Position = pos;

    uv0 = TEXCOORD_0;
    uv1 = TEXCOORD_1;
	   color = COLOR;

//// find distance from the camera

#if defined(FOG) || defined(NEAR_WATER)
		#ifdef FANCY
		vec3 relPos = VIEW_POS - (POSITION.xyz + CHUNK_ORIGIN);
		float cameraDepth = length(relPos);
	#else
		float cameraDepth = pos.z;
	#endif
#endif

#ifdef ALPHA_TEST
#ifdef FANCY 

if(color.g + color.r > color.b + color.b && abs(color.g - color.r) < 9.0) {
if (wavyLeaves) {

  vec3 wav = POSITION.xyz + CHUNK_ORIGIN;

	const float LeavesSpeed = 80.0;

 gl_Position.x += cos(TIME+((wav.y+wav.y + wav.x)*cos(wav.x)))/LeavesSpeed;
 gl_Position.y +=  cos(TIME+((wav.y+wav.y + wav.x)*cos(wav.z)))/LeavesSpeed;

//sin(TIME+(wav.xxy*sin(wav.z)))/500.8 - 0.05;
}
}
#endif
#endif

///// apply fog

#ifdef FOG
	float len = cameraDepth / RENDER_DISTANCE;
	#ifdef ALLOW_FADE
		len += CURRENT_COLOR.r;
	#endif
  fogColor.rgb = FOG_COLOR.rgb;
	fogColor.a = clamp((len - FOG_CONTROL.x) + (FOG_CONTROL.y - FOG_CONTROL.x), 0.0, 1.0);
#endif

///// water magic
#ifdef NEAR_WATER
	#ifdef FANCY  /////enhance water

		float F = dot(normalize(relPos), UNIT_Y);
   //F = dot(normalize(relPos), unit_mx);;
		//if both NEAR_WATER and FOG are defined, it's forced fog and we're underwater

		#ifndef FOG  //over water
		 F = 0.0 - max(F, 0.1);
		#else //under water
			color.a = 0.9; //disable fake transparency and be transparent
		#endif

//fogColor.rgb = FOG_COLOR.rgb;

		F = 1.0 - mix(F*F*F*F, 1.0, min(1.0, cameraDepth / FAR_CHUNKS_DISTANCE));

		color.rg -= vec2(F * DIST_DESATURATION);

		vec4 depthColor = vec4(color.rgb * 0.5, 1.0);
		vec4 traspColor = vec4(color.rgb * 0.45, 0.8);
		vec4 surfColor = vec4(color.rgb, 1.0);
   vec4 ender = vec4 (0.4,0.6,0.8,1.0);
   vec4 nearColor = mix(traspColor, depthColor, color.a);

const float fogNear = 0.0;
const float blurNear =0.0;

	float depthxy = pos.z / RENDER_DISTANCE;
 	float fogxy = max(depthxy - fogNear, 0.1);

if (WaterColor) {
color = ender * nearColor *color.b;
color.a = 0.7 - fogxy;
}

vec4 light = texture2D( TEXTURE_1, uv1 );
vec4 tex = texture2D( TEXTURE_0, uv0);

vec3 wav = VIEW_POS / (POSITION.xyz + CHUNK_ORIGIN);

vec3 wav2 = POSITION.xyz + CHUNK_ORIGIN;

vec2 text = floor(vec2(uv0.x * 32.0, uv0.y * 16.0));

//Wavy Water 

	const float waveHeight 		= 0.035;
	const float waveSpeed		= 3.141569;

//Wavy shadows
 const float shadowSpeed = 2.85;
 const float shadowResolution = 95.85;

//Wavy alpha
 const float alphaHeight = 0.04;
 const float alphaSpeed = 5.0;

	if (wavyWater) {
if ( text.x < 50.0){
gl_Position.y += waveHeight * cos(TIME * waveSpeed + wav2.x);

gl_Position.y += waveHeight * cos(TIME * waveSpeed + wav2.z);
  }
}

//Wavy shadows extras
	if (wavyWaterShadows) {

color.rgb += cos(TIME * shadowSpeed+((wav2.x+ wav2.z + wav2.x + wav2.z +wav2.x +wav2.x + wav2.y +wav2.x)*cos(wav2.z)))/55.0;

color.rgb += cos(TIME * shadowSpeed+((wav2.x+ wav2.z + wav2.x + wav2.z +wav2.x +wav2.x + wav2.y +wav2.x)*cos(wav2.x)))/55.0;
}

if (wavyWaterAlpha) {
color.a +=alphaHeight * sin(TIME * alphaSpeed + wav2.x)/5.0;
color.a +=alphaHeight * sin(TIME * alphaSpeed + wav2.z)/5.0;
}

//sin(TIME+(wav.xxy*sin(wav.z)))/500.8 - 0.05;

#else
		color.a = pos.z / FAR_CHUNKS_DISTANCE;
	#endif //FANCY
#else
color.b *= 0.78;
color = min(vec4(color.rgb * 1.0, 1.0), 1.0);
color.b *= 2.0;
#endif
}


