# EVO Shader Mod V1.2 "REAL ENGINE" (MCPE 1.8+)

![1703125682217](documentations/image/README/1703125682217.png)

# **EVO V1.2 ChangeLog Feature :**

*"Contain Old EVO Shader Feature"*

1. Add realistic "static" lighting
2. Add realistic Fog on distance
3. Add realistic torch light color
4. Add "Filmic-Tonemap" for realistic looking
5. Add optimize color for the world
6. Add heat fog on nether
7. Add realistic condition on rain (fog,etc)
8. Add realistic atmosphere sun and moon
9. Add optimize semi-3D realistic smooth cloud
10. Add natural blue color and smooth water on distance
11. Add realistic in-room ambience
12. Add adaptive torch-light near shadow
13. Add smooh darkness on shadow
14. Add realistic day,sunrise/sunset,night lighting
15. Add adaptive brightness on outside when inside shadow
16. As Always " Find and search more ![:)](https://media.minecraftforum.net/avatars/0/14/635356669596273706.png ":)") "

**Compability Issue / BUG / BUG-FIX List :**

1. There's compability issue on PowerVR G6200
2. Fix Adreno 5XX crash on game
3. Leaves is a big issue for performance
4. Rail 50% transparent for unknown reason

============================================================================

Important Setting ! (Locate inside /games/resource_packs/myshader/shaders/glsl/settings.h) :

#define WAVY 1 //set this to 1 for enable wavy shader, and 0 for disable

============================================================================

**Some Advice :**

1.Please turn MCPE brightness to "0" to make sure there's no lighting issue in-game

2.Adjust your phone brightness to good bright for good gameplay


**Useful information :**

1.Please wait for shader to load on your mcpe at least 1 minute from loading screen

(if not load that's mean shader is not support on your device)

please contact me about that.

2.Please read "EVO Lisence.txt" inside the shader if you want do something with shader "copy/paste or etc"


**Screenshot :**

![1703125971070](documentations/image/README/1703125971070.png)

![1703125978361](documentations/image/README/1703125978361.png)

![1703125984941](documentations/image/README/1703125984941.png)

![1703125991536](documentations/image/README/1703125991536.png)

![1703125997473](documentations/image/README/1703125997473.png)

**How To Install Shader :**

-Open file-explorer and click on .mcpack file, it will installed automatically on your mcpe.

**System Requirement :**

```
Check The Your Phone GFLOPS : CHECK

-if you can't find your gpu. search it on wiki according to your gpu.


System Requirement (Minimum) :

OS: Android/IOS

GFLOPS : 50-130


System Requirement (Recommend) :

OS: Android/IOS

GFLOPS : 130+
```

***CONTACT : @TRXDev***
